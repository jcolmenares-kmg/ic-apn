package com.digitel;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.google.common.base.Predicates;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
//@PropertySources({
//	@PropertySource("file:/opt/tomcat/apache-tomcat-8.5.37/webapps/intercorp/properties/ic-authenticator.properties"),
//	@PropertySource("file:/opt/tomcat/apache-tomcat-8.5.37/webapps/intercorp/properties/intercorpError.properties")
//			})
@PropertySources({
@PropertySource("classpath:ic-apn.properties")
})
public class ConfigAdapter extends WebMvcConfigurationSupport {
	
	private static final Logger logger = LogManager.getLogger(ConfigAdapter.class);
	//public static HashMap<String,String> mapErrorIc ;
	public static HashMap<String,String> mapConfAuthenticator;
	
	/*@Override
	public void addInterceptors(InterceptorRegistry registry) {
		System.out.println("Config Interceptor");
		registry.addInterceptor(new Interceptor())
		.excludePathPatterns(
				new String[] { 
						"/login","/v2/**", "/swagger-ui.html","/v2/api-docs?group=public-api", 
						"/swagger-resources", "/webjars/**", "/configuration/**",
						"/pruebas*","/renewal-login"
				});
	}*/
	
	/*@Override
	public void addCorsMappings(CorsRegistry registry) {
		System.out.println("Config Cors");*/
		//registry.addMapping("*/**")
		/*.allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD", "TRACE", "CONNECT")
		.allowedOrigins("*")
		.allowedHeaders("*")
		.exposedHeaders("Authorization")
		.allowCredentials(false);
	}*/
	
	@Bean
    public Docket api() { 
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("public-api")
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(Predicates.not(PathSelectors.regex("/error.*")))
				.build();                      
    }

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("InterCorp Backend")
				.description("InterCorp Backend Web Services")
				.license("JavaInUse License")
				.version("1.0")
				.build();
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
	@Autowired
	Environment env;
	@Bean
	public boolean readProrperties() throws Exception {
		
		StringBuilder sms = new StringBuilder();
		sms.append("Inicio Despliegue de ic-apn");
		logger.info(sms);	
		
		logger.info("Cargando Bean de errores ...read intercorpError.properties");
		//mapErrorIc = new HashMap<String, String>();
		mapConfAuthenticator = new HashMap<String,String>();
		
		/*mapErrorIc.put("error.cod_000", env.getProperty("error.cod_000").toString());
		mapErrorIc.put("error.cod_001", env.getProperty("error.cod_001").toString());
		mapErrorIc.put("error.cod_002", env.getProperty("error.cod_002").toString());
		mapErrorIc.put("error.cod_003", env.getProperty("error.cod_003").toString());
		mapErrorIc.put("error.cod_004", env.getProperty("error.cod_004").toString());
		mapErrorIc.put("error.cod_005", env.getProperty("error.cod_005").toString());
		mapErrorIc.put("error.cod_006", env.getProperty("error.cod_006").toString());*/
		
		logger.info("Cargando Bean de Configuración ... read ic-authenticator.properties");
		System.out.println(env.getProperty("bduc").toString());
		System.out.println(env.getProperty("get_apn_connection_type").toString());
		mapConfAuthenticator.put("bduc",env.getProperty("bduc").toString());
		mapConfAuthenticator.put("get_apn_connection_type",env.getProperty("get_apn_connection_type").toString());
		mapConfAuthenticator.put("ip.server",env.getProperty("ip.server").toString());
		/*mapConfAuthenticator.put("kenan",env.getProperty("kenan").toString());
		mapConfAuthenticator.put("expiryTime",env.getProperty("expiryTime").toString());
		mapConfAuthenticator.put("tokenSignature",env.getProperty("tokenSignature").toString());
		mapConfAuthenticator.put("validateUserApp",env.getProperty("validateUserApp").toString());
		mapConfAuthenticator.put("userFailledTry",env.getProperty("userFailledTry").toString());
		mapConfAuthenticator.put("get_configurationsSp",env.getProperty("get_configurationsSp").toString());
		
		mapConfAuthenticator.put("server",env.getProperty("server").toString());
		mapConfAuthenticator.put("port",env.getProperty("port").toString());
		mapConfAuthenticator.put("domain",env.getProperty("domain").toString());
		*/
		return true;

	}
	
}

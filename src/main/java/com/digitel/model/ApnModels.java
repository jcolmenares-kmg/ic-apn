package com.digitel.model;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.digitel.common.Connect;
import com.digitel.common.Response;
import com.digitel.ConfigAdapter;

import oracle.jdbc.OracleTypes;

public class ApnModels implements  Serializable {

	private static final long serialVersionUID = 233352197948000797L;
	private static final Logger logger = LogManager.getLogger(ApnModels.class);
	
	public Object getApnConfigurationType() throws Exception {
		
		StringBuilder sms = new StringBuilder();
		sms.append("Consulta de Configuraciones \n Model: ");
		sms.append(Response.getNameMethod());
		logger.info(sms.toString());
		
		Connection conn = null;
		CallableStatement pstmt = null;
		HashMap<String, Object> list = new HashMap<String, Object>();
		
		ResultSet result = null;
		int error_id = 0;
		String error_descr = null;
		
		try {
		
			conn = Connect.connectionOracle(ConfigAdapter.mapConfAuthenticator.get("bduc"));
			System.out.println("Si se conecto");
			result = null;
			error_id = 0;
			error_descr = null;
		
			try {
				
				pstmt = conn.prepareCall(ConfigAdapter.mapConfAuthenticator.get("get_apn_connection_type"));
				
				pstmt.registerOutParameter(1, OracleTypes.CURSOR);
				pstmt.registerOutParameter(2, Types.INTEGER);
				pstmt.registerOutParameter(3, Types.VARCHAR);
				
				pstmt.executeUpdate();
				System.out.println(pstmt);
				
				error_id = pstmt.getInt(2);
				error_descr = pstmt.getString(3);
				
				System.out.println(error_id);
				System.out.println(error_descr);
				
				if (error_id == 0) {
					
					result = (ResultSet) pstmt.getObject(1);
					
					while (result.next()) {
						list.put(result.getString(1), result.getString(2));
					}
					
					sms = new StringBuilder();
					sms.append("Respuesta: ");
					sms.append(error_descr);
					sms.append("\n");
					sms.append("Fin de la Consulta");
					logger.info(sms.toString() );
				}
				
			} catch (SQLException ex) {
				sms = new StringBuilder();
				sms.append("SQLException: ");
				sms.append(ex.getMessage());
				//logger.error(sms.toString());
				System.out.println("Se presento un error en la conexion");
				System.out.println(sms.toString());
				ex.printStackTrace();
			}
		
		} catch (SQLException ex) {
			sms = new StringBuilder();
			sms.append("SQLException: ");
			sms.append(ex.getMessage());
			//logger.error(sms.toString());
			System.out.println(sms.toString());
			ex.printStackTrace();
		}finally {
			pstmt.close();
			conn.close();
		}
		
		
		if (error_id != 0 ) {
			
			//GenericException ex = new  GenericException(Integer.toString(error_id), error_descr);
			logger.error("Error al obtener Instancias... ");
			//logger.error(ex);
			//throw ex;
			list.put("Error", "Error al obtener Instancias... ");
		}
		
		
		return list;
		
	}
}
//package 3 - esquema intercorp bduc getapntypeconeccion
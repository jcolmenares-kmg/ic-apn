package com.digitel.common;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.digitel.ConfigAdapter;

public class Connect implements Serializable {
	
	private static final long serialVersionUID = -5310616535386124320L;

public static Connection connectionOracle(String nameConection) throws Exception {

	Hashtable<String, String> ht = new Hashtable<String, String>();  
	ht.put(Context.INITIAL_CONTEXT_FACTORY,  "weblogic.jndi.WLInitialContextFactory");
	ht.put(Context.PROVIDER_URL, ConfigAdapter.mapConfAuthenticator.get("ip.server"));
	Context initContext = new InitialContext(ht);
	DataSource ds = (DataSource)initContext.lookup(nameConection);
	Connection conn = ds.getConnection();
	
	
	
	
	//Para usarlo en Tomcat
	/*Context initContext = new InitialContext();
	Context envContext  = (Context)initContext.lookup("java:/comp/env");
	DataSource ds = (DataSource)envContext.lookup(nameConection);
	Connection conn = ds.getConnection();*/
	
	return conn;
}
	
	
}

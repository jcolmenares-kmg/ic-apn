/**
 * 
 */
package com.digitel.common;

import java.io.Serializable;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.digitel.ConfigAdapter;

public class RechargeProperties implements Serializable{
	
	private static final long serialVersionUID = -3058880175477734512L;
	private static final Logger logger = LogManager.getLogger(RechargeProperties.class);

	static void rechargePropertie() throws Exception {
		
		Properties valuesProperties = ReadProperties.getProperties("ic-apn.properties");
		
		if (!valuesProperties.getProperty("bduc").toString().equals(ConfigAdapter.mapConfAuthenticator.get("bduc"))) {

			StringBuilder sms = new StringBuilder();
			sms.append("Cambio del parametro: bduc");
			sms.append(" Old: ");
			sms.append(ConfigAdapter.mapConfAuthenticator.get("bduc"));
			sms.append(" new: ");
			sms.append(valuesProperties.getProperty("bduc").toString());
			logger.debug(sms.toString());

			ConfigAdapter.mapConfAuthenticator.put("bduc", valuesProperties.getProperty("bduc").toString());
		}
		
		if (!valuesProperties.getProperty("get_apn_connection_type").toString().equals(ConfigAdapter.mapConfAuthenticator.get("get_apn_connection_type"))) {

			StringBuilder sms = new StringBuilder();
			sms.append("Cambio del parametro: get_apn_connection_type");
			sms.append(" Old: ");
			sms.append(ConfigAdapter.mapConfAuthenticator.get("get_apn_connection_type"));
			sms.append(" new: ");
			sms.append(valuesProperties.getProperty("get_apn_connection_type").toString());
			logger.debug(sms.toString());

			ConfigAdapter.mapConfAuthenticator.put("get_apn_connection_type", valuesProperties.getProperty("get_apn_connection_type").toString());
		}
		
		if (!valuesProperties.getProperty("ip.server").toString().equals(ConfigAdapter.mapConfAuthenticator.get("ip.server"))) {

			StringBuilder sms = new StringBuilder();
			sms.append("Cambio del parametro: ip.server");
			sms.append(" Old: ");
			sms.append(ConfigAdapter.mapConfAuthenticator.get("ip.server"));
			sms.append(" new: ");
			sms.append(valuesProperties.getProperty("ip.server").toString());
			logger.debug(sms.toString());

			ConfigAdapter.mapConfAuthenticator.put("ip.server", valuesProperties.getProperty("ip.server").toString());
		}
	}

}

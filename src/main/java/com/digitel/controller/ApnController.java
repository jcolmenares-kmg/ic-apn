package com.digitel.controller;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digitel.common.Response;
import com.digitel.model.ApnModels;

@RestController
public class ApnController implements Serializable {

	private static final long serialVersionUID = -5306014708650355646L;
	private static final Logger logger = LogManager.getLogger(ApnController.class);
	
	@RequestMapping(value="/get-apn-configuration-type", method = RequestMethod.GET)
	private Object GetApnConfigurationType(/*@RequestHeader String Authorization*/) throws Exception {
		
		StringBuilder sms = new StringBuilder();
		sms.append("Service: ");
		sms.append(Response.getNameMethod());
		//logger.info(sms.toString());*/
		
		return new Response(new ApnModels().getApnConfigurationType(), "200", "ok");
	}
}

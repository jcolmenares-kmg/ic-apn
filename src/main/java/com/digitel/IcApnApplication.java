package com.digitel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcApnApplication {

	public static void main(String[] args) {
		SpringApplication.run(IcApnApplication.class, args);
	}

}
